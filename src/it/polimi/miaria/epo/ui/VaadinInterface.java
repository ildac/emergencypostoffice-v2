package it.polimi.miaria.epo.ui;

import it.polimi.miaria.epo.core.DbConnector;
import it.polimi.miaria.epo.core.GMaps;
import it.polimi.miaria.epo.core.socialAggregator;
import it.polimi.miaria.epo.entities.CapBean;
import it.polimi.miaria.epo.utils.Cap;
import it.polimi.miaria.epo.utils.Post;
import it.polimi.miaria.epo.utils.Posts;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Set;

import javax.servlet.ServletContext;

import org.vaadin.hezamu.googlemapwidget.GoogleMap;
import org.vaadin.hezamu.googlemapwidget.GoogleMap.MapControl;
import org.vaadin.hezamu.googlemapwidget.overlay.BasicMarker;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.vaadin.Application;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.terminal.Sizeable;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class VaadinInterface extends Application implements Window.CloseListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Window mainWindow = new Window("Social Aggregator");
	HorizontalSplitPanel hsplit = new HorizontalSplitPanel(); // divisorio
																// verticale
	VerticalLayout left = new VerticalLayout(); // visualizzazione verticale a
												// sinistra
	VerticalLayout leftDown = new VerticalLayout();
	
	Window pubblicazione = new Window("Pubblica Post");
	Window cerca = new Window("Cerca sui social");
	Window dettaglio = new Window("Dettaglio CAP");
	VerticalLayout leftUp = new VerticalLayout();
	VerticalSplitPanel vsplit = new VerticalSplitPanel(); // divisione
															// orizzontale
															// della parte a
															// destra
	VerticalSplitPanel vsplit2 = new VerticalSplitPanel();// divisione del left
	VerticalSplitPanel vsplitmain = new VerticalSplitPanel();
	VerticalLayout rightUp = new VerticalLayout(); // pannello destro in
													// alto
	HorizontalLayout rightUpDown = new HorizontalLayout();// semipannello
															// inferiore di
															// rightUp
	TabSheet tab = new TabSheet(); // tab per la parte inferiore destra
	VerticalLayout mapsTab = new VerticalLayout(); // primo tab
	VerticalLayout postTab = new VerticalLayout(); // secondo tab
	VerticalLayout detailsTab = new VerticalLayout(); // terzo tab
	GoogleMap gMap = new GoogleMap(this); // mappa
	Table listaPost = new Table(); // tabella che mostra i risultati della
									// ricerca
	Table details = new Table(); // tabella dettaglio CAP
	GMaps gMaps = new GMaps();
	TextArea publish = new TextArea("PUBBLICA POST");
	Label coord1 = new Label();
	String capSelezionato;
	Cap cap;
	DbConnector conn = new DbConnector();
	ArrayList<CapBean> elenco_cap = conn.getInitialCaps();
	socialAggregator social = new socialAggregator();
	Posts container;
	GridLayout labelgrid = new GridLayout(2, 4);
	GridLayout dettgrid = new GridLayout(2, 15);
	Panel panel = new Panel("INFORMAZIONI SUL CAP");
	Button cercaPost = new Button("CERCA POST");
	Button pubblicaPost = new Button("PUBBLICA");
	OptionGroup optiongroup = new OptionGroup("Scegli il social");
	OptionGroup optiongroup2 = new OptionGroup("Scegli il social");
	ServletContext sc;
	MenuBar menubar = new MenuBar();

	@SuppressWarnings("serial")
	ClickListener buttonclick = new ClickListener() {

		@SuppressWarnings("unchecked")
		@Override
		public void buttonClick(ClickEvent event) {
			/*ArrayList<PostResultBean> db = new ArrayList<PostResultBean>( FacadeFactory.getFacade().list(PostResultBean.class));
			for (PostResultBean p : db) {
				System.out.println(p.getHash());
			}
			db.get(939).setPerc(50);
			FacadeFactory.getFacade().store(db.get(939));
			ArrayList<ViewPost> db = new ArrayList<ViewPost>( FacadeFactory.getFacade().list(ViewPost.class));
			double n=0,k=0,t=0;
			PostResultBean pr = null;
			for (ViewPost p : db) {
				
				if(p.getRilevante()==1)n++;
				pr = conn.getHash(p.getHash());
				t=(n/(k+1))*100;
				pr.setPerc(t);
				FacadeFactory.getFacade().store(pr);
				k++;
			}*/
			String t = "";
			Set<String> s = (Set<String>) optiongroup.getValue();
			for (String string : s) {
				t = t.concat(string);

			}
			container = new Posts();/*
			Test t2 = new Test();
			ArrayList<PostBean> dbPost = conn.getPost();
			ArrayList<TerminiBean> terms = conn.getTerms("Bristol");
			//terms = t2.pesi(terms.subList(0, terms.size()), "abuse");
			terms = t2.main("abuse");
			VectorSpaceModel vsm;
			for (PostBean p : dbPost) {
				container.addPost(p.getNome(), null, p.getTesto(), p.getData(),
						null, null, null, p.getSocial());
			}
			vsm = new VectorSpaceModel(container, terms);
			for (Post p : container.getSet()) {
				String plaintext = p.getText(), hashtext = null;
				MessageDigest m;
				try {
					m = MessageDigest.getInstance("MD5");

					m.reset();
					m.update(plaintext.getBytes());
					byte[] digest = m.digest();
					BigInteger bigInt = new BigInteger(1, digest);
					hashtext = bigInt.toString(16);
					// Now we need to zero pad it if you actually want the full
					// 32 chars.
					while (hashtext.length() < 32) {
						hashtext = "0" + hashtext;
					}
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				PostResultBean pr = new PostResultBean();
				pr.setHash(hashtext);
				pr.setSimilaritÓ(p.getSimilarita());
				FacadeFactory.getFacade().store(pr);

			}
			System.out.println("FINITO!!!");*/

			 container = social.search(t, conn.getTerms(capSelezionato));
			 /*container = social.search(t,conn.getTerms("Bristol") );
			 //container = social.search(t,t1.main("earthquake"));

			
			 ArrayList<TerminiBean> arg = new ArrayList<TerminiBean>();
			 TerminiBean t1 = new TerminiBean(); t1.setEvento("abuse");
			  t1.setTermine("ciao"); arg.add(t1); container = social.search(t,
			  arg); for (Post p : container.getSet()) { String plaintext =
			  p.getText(), hashtext = null; MessageDigest m; try { m =
			  MessageDigest.getInstance("MD5");
			  
			  m.reset(); m.update(plaintext.getBytes()); byte[] digest =
			  m.digest(); BigInteger bigInt = new BigInteger(1, digest);
			  hashtext = bigInt.toString(16); // Now we need to zero pad it if
			  you actually want the full 32 chars. while (hashtext.length() <
			  32) { hashtext = "0" + hashtext; } } catch
			  (NoSuchAlgorithmException e) { // TODO Auto-generated catch block
			  e.printStackTrace(); } PostBean pb = new PostBean();
			  pb.setData(p.getData()); pb.setNome(p.getNome()); pb.setPeso(0);
			  pb.setRilevante(0); pb.setTesto(p.getText());
			  pb.setHash(hashtext); pb.setSocial(p.getSocial());
			  FacadeFactory.getFacade().store(pb); } */
			  this.showPost();
			  optiongroup.setValue(""); System.out.println("FINITO!!!");
			 
		}

		private void showPost() {
			for (Post p : container.getSet()) {
				String[] cells = new String[] { p.getText(), p.getNome(),
						p.getSocial(),Double.toString(p.getSimilarita()) };
				listaPost.addItem(cells, container.getSet().indexOf(p));
			}
			postTab.requestRepaintAll();
		}
	};

	@SuppressWarnings("serial")
	ClickListener pubblicaclick = new ClickListener() {

		@SuppressWarnings("unchecked")
		@Override
		public void buttonClick(ClickEvent event) {
			social.getSettings().setPublishText((String) publish.getValue());
			String t = "";
			Set<String> s = (Set<String>) optiongroup2.getValue();
			for (String string : s) {
				t = t.concat(string);

			}
			social.publish(t);
			publish.setValue("");
			optiongroup2.setValue("");
			mainWindow.removeWindow(pubblicazione);

		}

	};

	@SuppressWarnings("serial")
	ItemClickListener treeclick = new ItemClickListener() {

		@Override
		public void itemClick(final ItemClickEvent event) {
			// getMainWindow().showNotification("Notification",
			// event.getItemId().toString(),
			// Notification.TYPE_WARNING_MESSAGE);

			social.inizializeCap(((CapBean) event.getItemId()).getPath());
			this.infoCap((CapBean) event.getItemId(), social.getCap());
			capSelezionato = social.getCap().getProprieta("event");
			cap = social.getCap();

		}

		private void infoCap(CapBean bean, Cap cap) {
			labelgrid.getComponent(1, 0).setCaption(bean.toString());
			labelgrid.getComponent(1, 1).setCaption(bean.getData().toString());
			labelgrid.getComponent(1, 2).setCaption(
					cap.getProprieta("senderName"));
			labelgrid.removeComponent(1, 3);
			Label l = new Label(cap.getProprieta("description"));
			l.setWidth("800");
			labelgrid.addComponent(l, 1, 3);
			labelgrid.setVisible(true);
			labelgrid.requestRepaint();
			rightUp.requestRepaintAll();
			cercaPost.setVisible(true);
			optiongroup.setVisible(true);
		}
	};
	CloseListener closeListener = new CloseListener() {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void windowClose(CloseEvent e) {
			if (e.getWindow().getCaption()== "Cerca sui social")
				listaPost.removeAllItems();
			else if (e.getWindow().getCaption()=="Pubblica Post"){
				optiongroup2.setValue("");
				
				publish.setValue("");
			}
			else if(e.getWindow().getCaption()=="Dettaglio CAP"){
				dettgrid.removeAllComponents();
			}
				
				
			
		}
	};

	@Override
	public void init() {
		buildLayout();

	}

	private void buildLayout() {

		setMainWindow(mainWindow); // creazione finestra principale
		
		mainWindow.setContent(vsplitmain);// aggiunta alla finestra principale
		vsplitmain.setFirstComponent(menubar);
		vsplitmain.setSecondComponent(hsplit);
		vsplitmain.setLocked(true);
		vsplitmain.setSplitPosition(23,Sizeable.UNITS_PIXELS);
		pubblicazione.center();
		pubblicazione.setResizable(false);
		pubblicazione.setHeight(270, Sizeable.UNITS_PIXELS);
		pubblicazione.setWidth(380, Sizeable.UNITS_PIXELS);
		pubblicazione.addListener(closeListener);
		cerca.setResizable(false);
		cerca.setHeight(600, Sizeable.UNITS_PIXELS);
		cerca.setWidth(1200, Sizeable.UNITS_PIXELS);
		VerticalSplitPanel cercaSplit = new VerticalSplitPanel();
		VerticalLayout hl = new VerticalLayout();
		hl.addComponent(optiongroup);
		hl.addComponent(cercaPost);
		cercaSplit.setSplitPosition(150, Sizeable.UNITS_PIXELS);
		cercaSplit.setLocked(true);
		cercaSplit.setFirstComponent(hl);
		cercaSplit.setSizeFull();
		//listaPost.setWidth(1000, Sizeable.UNITS_PIXELS);
		listaPost.addContainerProperty("Testo", String.class, null);
		listaPost.addContainerProperty("Autore", String.class, null);
		listaPost.addContainerProperty("Social", String.class, null);
		listaPost.addContainerProperty("Rilevanza", String.class, null);
		listaPost.setColumnWidth("Testo", 870);
		listaPost.setColumnWidth("Autore", 120);
		listaPost.setColumnWidth("Social", 60);
		listaPost.setColumnWidth("Rilevanza", 80);
		listaPost.setSizeFull();
		cercaSplit.setSecondComponent(listaPost);
		cerca.addListener(closeListener);
		cerca.setContent(cercaSplit);
		
		
		
		dettaglio.setResizable(false);
		dettaglio.setHeight(500, Sizeable.UNITS_PIXELS);
		dettaglio.setWidth(850, Sizeable.UNITS_PIXELS);
		dettaglio.addListener(closeListener);
			
		Command command = new Command() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void menuSelected(MenuItem selectedItem) {
				mainWindow.addWindow(pubblicazione);
				
			}
		};;;
		MenuItem i1 = menubar.addItem("Pubblica Post", null, command );
		
        Command command2 = new Command() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void menuSelected(MenuItem selectedItem) {
				mainWindow.addWindow(cerca);
				
			}
		};;;
		MenuItem i2 = menubar.addItem("Cerca sui social", null, command2 );
		
		Command command3 = new Command() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void menuSelected(MenuItem selectedItem) {
				Enumeration e = cap.getProp().propertyNames();
				int col=0,raw=0;
				
			    while (e.hasMoreElements()) {
			      String key = (String) e.nextElement();
			      System.out.println(key);
			      System.out.println(raw);
			      if(key!="path"){
			    	  
			      dettgrid.addComponent(new Label(key), col, raw);
			      col = 1;
			      Label l = new Label(cap.getProprieta(key));
			      l.setWidth("750");
			      dettgrid.addComponent(l, col, raw);
			      col = 0;
			      raw++;
			      }
			    }
			    dettgrid.setMargin(true);
				dettaglio.setContent(dettgrid);
				mainWindow.addWindow(dettaglio);
				
			}
		};;;
		MenuItem i3 = menubar.addItem("Dettaglio CAP", null, command3 );
		// ----SINISTRA----

		Tree tree = new Tree("ELENCO CAP");
		for (CapBean c : elenco_cap) {

			tree.addItem(c);

		}

		tree.setImmediate(true);
		tree.addListener(treeclick);
		menubar.setWidth(1366, Sizeable.UNITS_PIXELS);
		menubar.setHeight(23, Sizeable.UNITS_PIXELS);
		pubblicaPost.addListener(pubblicaclick);
		publish.setWidth("300");
		optiongroup2.addItem("Facebook");
		optiongroup2.addItem("Twitter");
		optiongroup2.setMultiSelect(true);
		left.setSizeFull();
		//vsplit2.setFirstComponent(leftUp);
		//vsplit2.setSecondComponent(leftDown);
		leftUp.addComponent(tree);
		leftUp.setMargin(true);
		left.addComponent(leftUp);
		leftDown.addComponent(publish);
		leftDown.addComponent(optiongroup2);
		leftDown.addComponent(pubblicaPost);
		leftDown.setMargin(true);
		pubblicazione.setContent(leftDown);
		// leftDown.setHeight(50,Sizeable.UNITS_PERCENTAGE);

		hsplit.setSplitPosition(25, Sizeable.UNITS_PERCENTAGE); // setto lo
																// split al 25%
																// della
																// finestra
		hsplit.setFirstComponent(left); // aggiunto nella parte sinistra della
										// finestra

		// ----DESTRA----
		hsplit.setSecondComponent(vsplit); // aggiunto nella parte destra della
											// finestra

		// destra alto

		/*
		 * bt.addListener(null, new Button.ClickEvent(bt) {
		 * 
		 * @Override public void buttonClick(ClickEvent event) {
		 * 
		 * } }, null);
		 */

		// cercaPost.setVisible(false);
		// labelgrid.setVisible(false);
		Panel p = new Panel();
		p.setWidth("500");

		Label l = new Label("");
		p.addComponent(l);
		// l.setWidth("500");
		labelgrid.setImmediate(true);
		labelgrid.setMargin(true);
		labelgrid.addComponent(new Label("EVENTO:"), 0, 0);
		labelgrid.addComponent(new Label(""), 1, 0);
		labelgrid.addComponent(new Label("DATA:"), 0, 1);
		labelgrid.addComponent(new Label(""), 1, 1);
		labelgrid.addComponent(new Label("MITTENTE:"), 0, 2);
		labelgrid.addComponent(new Label(""), 1, 2);
		labelgrid.addComponent(new Label("DESCRIZIONE:"), 0, 3);
		labelgrid.addComponent(new Label(""), 1, 3);
		cercaPost.addListener(buttonclick);
		optiongroup.addItem("Facebook");
		optiongroup.addItem("Twitter");
		optiongroup.setMultiSelect(true);

		// rightUp.addComponent(coord1);
		panel.addComponent(labelgrid);
		rightUp.addComponent(labelgrid);
		// rightUp.addComponent(rightUpDown);
		//rightUp.addComponent(optiongroup);
		//rightUp.addComponent(cercaPost);

		vsplit.addComponent(rightUp); // aggiunto un layout orrizzontale per la
		vsplit.setWidth("1000"); // parte superiore
		vsplit.setSplitPosition(150	, Sizeable.UNITS_PIXELS);
		vsplit.setLocked(true);
		// destra basso -> Tabs
		vsplit.addComponent(tab);

		// primo tab
		mapsTab.setCaption("Maps");
		// mapsTab.addComponent(new Label("tab mappa"));
		mapsTab.setSizeFull();
		gMap.setWidth("1000");
		gMap.setHeight("380");
		gMap.addControl(MapControl.MenuMapTypeControl);
		gMap.addControl(MapControl.ScaleControl);
		gMap.addControl(MapControl.LargeMapControl);
		// gMap.getOverlays();
		gMap.addMarker(new BasicMarker(1L,
				new Point2D.Double(-115.52944,32.97861), "Test marker"));
		gMap.setCenter(new Point2D.Double(-115.52944,32.97861));

		mapsTab.addComponent(gMap);
		tab.addComponent(mapsTab);

		// secondo tab
		//postTab.setCaption("Post");
		//postTab.setImmediate(true);
		// imposto l'intestazione della tabella
		// listaPost.addContainerProperty("Data", String.class, null);
		
		//postTab.addComponent(listaPost);
		//tab.addComponent(postTab);

		// terzo tab
		

	}

	private void updatePostList() {
		// caricamento del contenuto della tabella dei post

	}

	private void loadCapDetails() {
		// caricamento dettaglio CAP
	}

	@Override
	public void windowClose(CloseEvent e) {
		// TODO Auto-generated method stub
		
	}

}