package it.polimi.miaria.epo.socialApi;

import java.util.GregorianCalendar;

import it.polimi.miaria.epo.utils.Cap;
import it.polimi.miaria.epo.utils.Posts;
import it.polimi.miaria.epo.utils.Settings;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.exception.FacebookException;
import com.restfb.exception.FacebookJsonMappingException;
import com.restfb.types.FacebookType;
import com.restfb.types.Post;

public class Facebook {
	@SuppressWarnings("unused")
	private Cap cap;
	private Posts container;
	private final FacebookClient publicFacebookClient, privateFacebookClient;
	private Connection<Post> publicSearch = null;
	private Settings settings;
	private GregorianCalendar d1 = new GregorianCalendar(2011, 4, 31);
	private GregorianCalendar d2 = new GregorianCalendar(2011, 5, 2);

	public Facebook(Settings s, Cap c, Posts p) {
		this.cap = c;
		this.container = p;
		this.publicFacebookClient = new DefaultFacebookClient();
		this.privateFacebookClient = new DefaultFacebookClient(
				"128455877182510|39fd253c57123efb2e2ba213.0-1531866467|IBMVeeDKE44llkOtXX1PByB25bw");
		this.settings = s;
	}

	public Posts search() {
		if (this.settings.capIsSet() && this.settings.isSetSearchText()) {
			try {
				publicSearch = publicFacebookClient.fetchConnection("search",
						Post.class,
						Parameter.with("q", settings.getSearchText()),
						//Parameter.with("q", "scandal"),
						//Parameter.with("q", "bristol"),
						//Parameter.with("q", "home"),
						//Parameter.with("limit", "800"),
						Parameter.with("type", "post"),
						//Parameter.with("since", d1.getTime()),
						//Parameter.with("until", d2.getTime()),
						Parameter.with("locale", "en_GB"));
			} catch (FacebookJsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FacebookException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.recursive(publicSearch, container,0);
			 //this.fillContainer();
			// this.print();
		}
		return this.container;
	}

	public Posts recursive(Connection<Post> ris, Posts con, int n) {

		for (Post p : ris.getData()) {
			if (p.getMessage() != null) {
				this.container.addPost(p.getFrom().getName(), p.getLink(),
						p.getMessage(), p.getCreatedTime(),null, null,
						null, "Facebook");
			}
		}
		n++;

		if (ris.hasNext() && container.getSet().size() <= 1000 && n<3)
			recursive(
					publicFacebookClient.fetchConnectionPage(
							ris.getNextPageUrl(), Post.class), container, n);
		return container;
	}

	public void fillContainer() {
		for (Post p : publicSearch.getData()) {
			if (p.getMessage() != null) {
			this.container.addPost(p.getFrom().getName(), p.getLink(),
					p.getMessage(), p.getCreatedTime(), null,null, null, "FACEBOOK");
		
			}
		}

	}

	public void publish() {
		try {
			this.privateFacebookClient.publish("me/feed", FacebookType.class,
					Parameter.with("message", settings.getPublishText()));
		} catch (FacebookJsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FacebookException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// public void print(){
	// container.stampaPost();
	// }

}
