package it.polimi.miaria.epo.socialApi;

import it.polimi.miaria.epo.utils.Cap;
import it.polimi.miaria.epo.utils.Posts;
import it.polimi.miaria.epo.utils.Settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.http.AccessToken;
import twitter4j.http.RequestToken;

public class TwitterApi {
	
	//private HTTPConnection http=null;
	private Settings settings;
	private Cap cap;
	private Posts posts;
	private TwitterFactory factory = new TwitterFactory();
    private Twitter twitter /*= new TwitterFactory().getInstance()*/;	// The factory instance is re-useable and thread safe.
	private Query query = new Query();
	private ConfigurationBuilder configuration = new ConfigurationBuilder();	//per gestire le configurazioni di Twitter4j proxy end co.
	private AccessToken accessToken;
	
	
	//quando costruisco devo passargli la connessione creata nel main
	public TwitterApi(Settings s, Cap c, Posts p) {
		this.settings=s;
		this.cap=c;
		this.posts=p;
		
		this.accessToken = TwitterApi.loadAccessToken(82838297);
		this.twitter = this.factory.getOAuthAuthorizedInstance("s9mNHetne08JigRo8LRE3A", "CablOqNKvczHaT4WRSgiRoElIJI3FVBgLpjpQLgM", this.accessToken);

		//controllo se ci sono impostazioni di settings allora configuro di conseguenza la libreria twitt4j
		if (this.settings.useProxy()) {
			configuration.setHttpProxyHost(this.settings.getProxyHost());
			configuration.setHttpProxyPort(this.settings.getProxyPort());
		}
	}
	
	private static AccessToken loadAccessToken(int useId){
		String token = "82838297-Uj8eS8Kh4TWwbnQKAWBe2jeW3X8yd1QRrRLW4o5fd";
		String tokenSecret = "aSSAgZO7a2V5lZxfTFihJ6e8NJfxZJZRUx5agcC0";
		return new AccessToken(token, tokenSecret);
	}
	
	/*	Questo metodo compone la query, la esegue, e la aggiunge al vettore dei post
	 * 	passato alla classe twitter, in questo modo tutti i post finiscono allo
	 * 	stesso posto.
	 */
	public Posts getPost() {
		this.search(this.twitter, this.makeQuery(cap, settings, query));
		return posts;
	}
	
	//estrae dal cap le informazioni utili seguendo i le impostazioni settate in precedenza
	private Query makeQuery(Cap c, Settings s, Query q) {
		if (s.capIsSet() && s.isSetSearchText()) {
			q.query(s.getSearchText());		//prendo il testo della ricerca nei settings
			//q.query("bristol+scandal");
			//q.setSince("2011-05-31");
			//q.setUntil("2011-06-02");
			
		} else {
			//TODO lanciare eccezione se il non c'è nessun parametro per la ricerca.
		}
		return query;
	}
	
	//questo metodo lancia la ricerca su twitter
	private void search (Twitter twitter, Query query) {
		try {
			QueryResult result = twitter.search(query);
		    for (Tweet tweet : result.getTweets()) {
		    	posts.addPost(tweet.getFromUser(), tweet.getSource(), tweet.getText(), tweet.getCreatedAt(),null, "" ,null, "Twitter");	        
		    }
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}
	
	//da usare solo per rinnovare l'access token
	public void login() throws TwitterException, IOException {
		// The factory instance is re-useable and thread safe.
		//Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer("s9mNHetne08JigRo8LRE3A", "CablOqNKvczHaT4WRSgiRoElIJI3FVBgLpjpQLgM");
		RequestToken requestToken = twitter.getOAuthRequestToken();
		AccessToken accessToken = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (null == accessToken) {
			System.out.println("Open the following URL and grant access to your account:");
			System.out.println(requestToken.getAuthorizationURL());
			System.out.print("Enter the PIN(if aviailable) or just hit enter.[PIN]:");
			String pin = br.readLine();
				try{
					if(pin.length() > 0){
						accessToken = twitter.getOAuthAccessToken(requestToken, pin);
			        }else{
			        	accessToken = twitter.getOAuthAccessToken();
			        }
			     } catch (TwitterException te) {
			    	 if(401 == te.getStatusCode()){
			         System.out.println("Unable to get the access token.");
			        }else{
			          te.printStackTrace();
			        }
			      }
			    }
			    //persist to the accessToken for future reference.
	    storeAccessToken(twitter.verifyCredentials().getId() , accessToken);
	    //Status status = twitter.updateStatus(args[0]);
	    //System.out.println("Successfully updated the status to [" + status.getText() + "].");
	    //System.exit(0);
	  }
	  private static void storeAccessToken(int useId, AccessToken accessToken){
		  System.out.println(useId);
		  System.out.println(accessToken.getToken());
		  System.out.println(accessToken.getTokenSecret());
	  }
	  
	//metodo per postare su twitter
	@SuppressWarnings("unused")
	public void post() {
		try {
			Status status = twitter.updateStatus(settings.getPublishText());
		} catch (TwitterException e) {
			System.out.println("Ops c'� un errore nella pubblicazione su Twitter");
			e.printStackTrace();
		}
	}

}
