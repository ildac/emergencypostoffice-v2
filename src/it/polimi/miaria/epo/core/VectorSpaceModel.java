package it.polimi.miaria.epo.core;

import it.polimi.miaria.epo.entities.TerminiBean;
import it.polimi.miaria.epo.utils.Post;
import it.polimi.miaria.epo.utils.Posts;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class VectorSpaceModel {
	private Posts posts;
	private ArrayList<TerminiBean> terms;

	public VectorSpaceModel(Posts p, ArrayList<TerminiBean> terms) {
		this.posts = p;
		this.terms = terms;

		this.calcolaVettori(this.posts, this.terms);
		this.calcolaRilevanza(this.posts, this.terms.size());

		Collections.sort(posts.getSet(), Collections.reverseOrder());
		Set<Post> s = new LinkedHashSet<Post>(posts.getSet());
		ArrayList<Post> temp = new ArrayList<Post>();
		for (Post p1 : s) {
			if (p1.getSimilarita() > 0 && p1.getText() != null)
				temp.add(p1);
		}
		posts.setSet(temp);

	}

	public Posts getPosts() {
		return this.posts;
	}

	private Posts calcolaRilevanza(Posts elenco, int n) {
		double[] query = new double[n];
		this.init(query, 1, terms);
		for (Post p : elenco.getSet()) {
			if (this.modulo(p.getFrequenze()) == 0)
				p.setSimilarita(0);
			else {
				MathContext mc = new MathContext(8, RoundingMode.HALF_UP);
				BigDecimal n1 = new BigDecimal(this.prodScalare(query,
						p.getFrequenze()));
				BigDecimal n2 = new BigDecimal(this.modulo(query)
						* this.modulo(p.getFrequenze()));
				p.setSimilarita(n1.divide(n2, mc).doubleValue());
			}
		}

		return elenco;

	}

	// ad ogni post viene assegnato il suo vettore di frequenze rispetto al
	// dizionario di termini
	private void calcolaVettori(Posts elenco, ArrayList<TerminiBean> terms2) {
		for (Post p : elenco.getSet()) {
			double[] freq = new double[terms2.size()];
			this.init(freq, 0,null);
			if (p.getText() != null) {
				StringTokenizer st = new StringTokenizer(p.getText());
				while (st.hasMoreTokens()) {
					String s = st.nextToken();
					for (TerminiBean t : terms2) {
						if (s.toLowerCase().contains(
								t.getTermine().toLowerCase())
								&& freq[terms2.indexOf(t)] == 0) {
							freq[terms2.indexOf(t)] = t.getPeso();
						}
					}
				}
			}
			p.setFrequenze(freq);
		}
	}

	private void init(double[] freq, int n, ArrayList<TerminiBean> t) {
		if (n == 0) {
			for (int i = 0; i < freq.length; i++) {
				freq[i] = 0;
			}
		} else {
			for (int i = 0; i < freq.length; i++) {
				freq[i] = t.get(i).getPeso();
			}
		}
	}

	private double modulo(double[] ds) {
		double n = 0;
		for (int i = 0; i < ds.length; i++) {
			n += Math.pow(ds[i], 2);

		}
		return Math.sqrt(n);

	}

	private double prodScalare(double[] query, double[] ds) {
		double ris = 0;
		for (int i = 0; i < query.length; i++) {
			ris += query[i] * ds[i];
		}
		return ris;

	}

}
