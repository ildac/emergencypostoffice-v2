package it.polimi.miaria.epo.core;

public class Coppia implements Comparable<Coppia> {
	private String par = null;
	private double pes = 0;

	public Coppia(String p, double pe) {
		this.par = p;
		this.pes = pe;
	}

	public String getText() {
		return par;
	}

	public double getPes() {
		return pes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if ((o == null) || (o.getClass() != this.getClass()))
			return false;
		Coppia p = (Coppia) o;
		if (p.getText() != null && par != null)
			return par.equals(p.getText()) && pes==p.getPes();
		else
			return false;

	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = (int) (31 * hash + pes);
		return hash;
	}

	@Override
	public int compareTo(Coppia p) {
		if (this.pes < p.getPes())
			return -1;
		else if (this.pes == p.getPes())
			return 0;
		else
			return 1;
	}
	
	@Override
	public String toString() {
		return par +": "+pes; 
	}
}

