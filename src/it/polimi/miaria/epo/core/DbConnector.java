package it.polimi.miaria.epo.core;

import it.polimi.miaria.epo.entities.CapBean;
import it.polimi.miaria.epo.entities.PostBean;
import it.polimi.miaria.epo.entities.PostResultBean;
import it.polimi.miaria.epo.entities.TerminiBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.vaadin.appfoundation.persistence.facade.FacadeFactory;

public class DbConnector  {

	public ArrayList<CapBean> getInitialCaps() {
		return new ArrayList<CapBean>(FacadeFactory.getFacade().list(
				CapBean.class));
	}

	public void putCap(CapBean c) {

		FacadeFactory.getFacade().store(c);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ArrayList<TerminiBean> getTerms(String evento) {
		String query = "SELECT t FROM TerminiBean AS t WHERE t.evento=:evento";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("evento", evento);
		return new ArrayList(FacadeFactory.getFacade().list(query, parameters));
	}
	
	public ArrayList<PostBean> getPost() {
		return new ArrayList<PostBean>(FacadeFactory.getFacade().list(
				PostBean.class));
	}
	
	public PostResultBean getHash(String hash){
		String query = "SELECT t FROM PostResultBean AS t WHERE t.hash=:hash";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("hash", hash);
		return FacadeFactory.getFacade().find(query, parameters);
	}

	

}
