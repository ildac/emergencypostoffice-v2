package it.polimi.miaria.epo.core;

import it.polimi.miaria.epo.utils.Location;

import java.io.IOException;
import java.net.URL;

import com.google.gson.Gson;
 

public class GMaps {

	Location loc = new Location();
	
	public void GMaps() {
		
	}
	
	public String getAdrs(String c)
    {
            String thisLine;
            String adrcoord = c;
            try {   
            adrcoord = adrcoord.replace(' ', '+');
            URL u = new URL("http://maps.google.com/maps/json?latlng=" + adrcoord + "&sensor=false");
            String json;
			
				json = u.getContent().toString();

            
            Gson gson = new Gson();
            loc = gson.fromJson(json, Location.class);
            
            return loc.getAdr();
            
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "error loc";
			}
    }
	
}
