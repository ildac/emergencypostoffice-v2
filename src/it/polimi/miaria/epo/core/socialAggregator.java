package it.polimi.miaria.epo.core;

import it.polimi.miaria.epo.entities.TerminiBean;
import it.polimi.miaria.epo.socialApi.Facebook;
import it.polimi.miaria.epo.socialApi.TwitterApi;
import it.polimi.miaria.epo.utils.Cap;
import it.polimi.miaria.epo.utils.CapRuleSet;
import it.polimi.miaria.epo.utils.Post;
import it.polimi.miaria.epo.utils.Posts;
import it.polimi.miaria.epo.utils.Settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

public class socialAggregator {
	private Settings settings; // possibilita di impostare vari parametri della libreria								
	private CapRuleSet crs;
	private Cap cap;
	private VectorSpaceModel vsm;

	public socialAggregator() {
		this.settings = new Settings();
		// settings.setProxy("131.175.12.65", 8080); //per settare il proxyPolimi,
		// funziona solo con twitter!
	}

	public void inizializeCap(String path) {
		// se necessario si possono aggiungere dei settings qui.
		this.settings.setCapDir(path);

		// a questo punto viene inzializzato il cap
		this.crs = new CapRuleSet();
		this.cap = crs.initialize(settings.getCapDir());
	}

	public Cap getCap() {
		return this.cap;
	}

	public Settings getSettings() {
		return this.settings;
	}

	public Posts search(String social, ArrayList<TerminiBean> terms) {
		//String[] termini = new String[terms.size()];
		
		Posts posts = new Posts(); // creo il vettore del post
		
		//cerco in base al social
		if (social.contains("Facebook")) {
			// creo l'oggetto facebook e lancio la ricerca
			for (TerminiBean t : terms) {
				this.settings.setSearchText(t.getTermine());
				Facebook face = new Facebook(settings, cap, posts);
				posts = face.search();
			}
		}
		
		if (social.contains("Twitter")) {
			// creo l'oggetto twitter e lancio la ricerca
			for (TerminiBean t : terms) {
				this.settings.setSearchText(t.getTermine());
				TwitterApi twitter = new TwitterApi(settings, cap, posts);
				posts = twitter.getPost();
			}
		}

		
		
		//applicazione del VectorSpaceModel
		vsm = new VectorSpaceModel(posts, terms);
		
		//ordinamento dei post in base alla rilevanza
		/*File f = new File("/Users/SUPERALEX/Workspace/bristol_abuse.txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*PrintStream ps = new PrintStream(fos);
		ps.println("TOTALE POST: "+posts.getSet().size());
		for (Post p1 : posts.getSet()) {
			ps.println("NOME: " + p1.getNome());
			ps.println("TESTO: " + p1.getText());
			ps.println("SOCIAL: " + p1.getSocial());
			ps.println("RILEVANZA: " + p1.getSimilarita());
			ps.println("DATA: "+p1.getData());
			ps.println("*********************************************************************");
			ps.println("*********************************************************************");
		}
	    try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return posts;
	}

	public void publish(String social) {
		Posts posts = new Posts(); // creo il vettore del post
		if (social.contains("Twitter")) {
			// creo l'oggetto twitter e lancio la pubblicazione
			TwitterApi twitter = new TwitterApi(settings, cap, posts);
			twitter.post();
		}

		if (social.contains("Facebook")) {
			// creo l'oggetto facebook e lancio la pubblicazione
			Facebook face = new Facebook(settings, cap, posts);
			face.publish();
		}

	}

}
