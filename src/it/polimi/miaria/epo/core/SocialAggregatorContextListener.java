package it.polimi.miaria.epo.core;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.vaadin.appfoundation.persistence.facade.FacadeFactory;

public class SocialAggregatorContextListener implements ServletContextListener {
	private DbConnector conn;
	

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// Setup and register the facade
		//this.conn = new DbConnector();
		try {
			FacadeFactory.registerFacade("default", true);
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public DbConnector getConn() {
		return conn;
	}

	
	

}
