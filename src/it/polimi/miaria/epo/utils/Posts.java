package it.polimi.miaria.epo.utils;

import java.util.ArrayList;
import java.util.Date;

/*	Questa classe è solo un contenitore di oggetti post.
 * 	Implementa funzioni di ricerca, ad esempio:
 * 		- ricerca in base ai geotag se sono vicini alla zona della calamità
 * 		- ricerca in base ai geotag presenti
 * 		- ricerca in base alla data
 * 	
 */
public class Posts {

	private ArrayList<Post> set;

	public Posts() {
		set = new ArrayList<Post>();
	}

	public void addPost(String nome, String link, String testo,Date data, String tags,
			String links, String geoLoc, String social) {
		set.add(new Post(nome, link, testo,data, tags, links, geoLoc, social));
	}

	public void stampaPost() {
		for (Post p : set) {
			System.out.println("NOME: " + p.getNome());
			System.out.println("TESTO: " + p.getText());
			System.out.println("SOCIAL: " + p.getSocial());
			System.out.println("RILEVANZA: " + p.getSimilarita());
			if (p.getFrequenze() != null) {
				System.out.println("[" + p.getFrequenze()[0] + ","
						+ p.getFrequenze()[1] + "," + p.getFrequenze()[2] + ","
						+ p.getFrequenze()[3]+"]");
			}
			System.out.println("******************************");
		}
	}

	public void setSet(ArrayList<Post> set) {
		this.set = set;
	}

	public ArrayList<Post> getSet() {
		return set;
	}

}
