package it.polimi.miaria.epo.utils;

import java.util.*;

/*	questa classe è l'astrazione java di un C.A.P. XML, i parametri che interessano
 *	qui vengono messi solo i parametri che interessano ai fini dell'applicazione
 *	per la mappatura viene utilizzata la libreria Digester del progetto Apache
 *	http://commons.apache.org/digester/
 */

public class Cap {

	private Properties prop;

	// private HashMap<String,String> proprieta; //parametri del CAP

	// mi interessano
	// identifier
	// sender
	// sent
	// scope - public private etc etc
	// area->circle
	// info->event
	// info->headline
	// info->description

	public Cap() {
		this.prop = new Properties();
		// this.proprieta = new HashMap<String, String>();
	}
	
	public Properties getProp(){
		return prop;
	}

	private void addProprieta(String nome, String val) {
		this.prop.put(nome, val);
		// this.proprieta.put(nome, val);
	}

	public String getProprieta(String nome) {
		return (String) this.prop.get(nome);
		// return this.proprieta.get(nome);
	}

	// setter per le proprietà che mi interessano
	// identifier
	// sender
	// sent
	// scope - public private etc etc
	// area->circle
	// info->event
	// info->headline
	// info->description
	// info->category

	public void addPath(String path) {
		this.addProprieta("path", path);
	}

	public void setIdentifier(String valore) {
		this.addProprieta("identifier", valore);
	}

	public void setSender(String valore) {
		this.addProprieta("sender", valore);
	}

	public void setSent(String valore) {
		this.addProprieta("sent", valore);
	}

	public void setScope(String valore) {
		this.addProprieta("scope", valore);
	}

	public void setAreaDesc(String valore) {
		this.addProprieta("areaDesc", valore);
	}

	public void setCircle(String valore) {
		this.addProprieta("circle", valore);
	}

	public void setEvent(String valore) {
		this.addProprieta("event", valore);
	}

	public void setHeadline(String valore) {
		this.addProprieta("headline", valore);
	}

	public void setDescription(String valore) {
		this.addProprieta("description", valore);
	}

	public void setCategory(String valore) {
		this.addProprieta("category", valore);
	}
	
	public void setUrgency(String valore) {
		this.addProprieta("urgency", valore);
	}
	
	public void setSeverity(String valore) {
		this.addProprieta("severity", valore);
	}
	
	public void setCertainty(String valore) {
		this.addProprieta("certainty", valore);
	}
	
	public void setSenderName(String valore) {
		this.addProprieta("senderName", valore);
	}
	
	public void setWeb(String valore) {
		this.addProprieta("web", valore);
	}
	
	
}