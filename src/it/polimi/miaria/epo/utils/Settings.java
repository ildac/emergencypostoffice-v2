package it.polimi.miaria.epo.utils;
/*
 * In questa classe si raccolgono le impostazoni globali:
 * Connessione:
 * 	- proxy
 * Sistema:
 * 	- posizione del file XML CAP se presente,
 * 	- i parametri di ricerca se non è settato il percorso del CAP
 * Query:
 * 	- i parametri secondo cui deve essere creata la query
 * 		- testo,
 * 		- geo,
 * 		- nome utente,
 * 		- località
 */


public class Settings {
	
	//connessione
	//TODO implementare proxy per Libreria facebook.
	private boolean useProxy;
	private String proxyHost;
	private int proxyPort;
	//percorso file CAP
	private String capDir;
	//Search Param
	private String searchText,publishText;		//stringa da utilizzare se non è disponibile un CAP
	private boolean searchGeo;		//cerca utilizzando i parametri della Geolocalizzazione - NON IMPLEMENTATO
	private boolean searchUser;		//cerca utilizzando uno specifico username - NON IMPLEMENTATO
	private boolean searchLocation;	//cerca i tweet provenienti da una specifica zona,
									//basandosi sui dati geografici del profilo utente se dispobinili
									//NON IMPLEMENTATO
	
	public Settings() {
		//inizializzazione dell'oggetto con i parametri di default!
		this.useProxy = false;				
		this.proxyHost = null;
		this.proxyPort = 0;
		this.capDir = null;
		this.searchText = null;
		this.searchGeo = false;
		this.searchUser = false;
		this.searchLocation = false;
	}
	
	public void setCapDir(String d) {
		this.capDir = d;
	}
	
	public void setSearchText(String t) {
		this.searchText = t;
	}
	
	public void setSearchGeo(boolean b) {
		this.searchGeo = b;
	}
	
	public void setSearchUser(boolean b) {
		this.searchUser = b;
	}
	
	public void setSearchLocation(boolean b) {
		this.searchLocation = b;
	}
	
	public void setProxy(String address, int port) {
		this.proxyHost = address;
		this.proxyPort = port;
		this.useProxy = true;
	}
	
	public void unSetProxy () {
		this.proxyHost = null;
		this.proxyPort = 0;
		this.useProxy = false;
	}
	
	//controlla se è stato aggiunto un file XML CAP
	//TODO si potrebbe controllare se il file esiste
	public boolean capIsSet() {
		if (this.capDir.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	public String getCapDir() {
		return this.capDir;
	}
	
	//controlla che sia stato inserito un testo da cercare
	//TODO si potrebbero controllare i termini inseriti che non siano cose strane
	public boolean isSetSearchText() {
		if (this.searchText.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	public String getSearchText () {
		return this.searchText;
	}
	
	public boolean searchGeo() {
		return this.searchGeo;
	}
	
	public boolean searchUser() {
		return this.searchUser;
	}
	
	public boolean searchLocation() {
		return this.searchLocation;
	}
	
	public boolean useProxy() {
		return this.useProxy;
	}
	
	public String getProxyHost() {
		return this.proxyHost;
	}
	
	public int getProxyPort() {
		return this.proxyPort;
	}

	public void setPublishText(String publishText) {
		this.publishText = publishText;
	}

	public String getPublishText() {
		return publishText;
	}
}
