package it.polimi.miaria.epo.utils;

import java.util.Date;

/*	Questo oggetto ha il compito di mappare un post proveniente da qualsiasi social
 * 	tutti i post hanno caratteristiche comuni:
 * 		- nome utente
 * 		- link all'originale
 * 		- testo
 * 		- tags
 * 		- immagini? twitpic?
 * 		- link
 * 		- data pubblicazione
 * 		- ora pubblicazione
 */

public class Post implements Comparable<Post> {
	private String nome, link, text, tags, links, geoLoc, social;
	private Date data;
	private double[] frequenze;
	private double similarita = 0;

	Post(String nome, String link, String text,Date data, String tags, String links,
			String geoLoc, String social) {
		this.setNome(nome);
		this.setLink(link);
		this.setText(text);
		this.setTags(tags);
		this.setLinks(links);
		this.setGeoLoc(geoLoc);
		this.setSocial(social);
		this.setData(data);
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getNome() {
		return nome;
	}

	public String getLink() {
		return link;
	}

	public String getText() {
		return text;
	}

	public String getTags() {
		return tags;
	}

	public String getLinks() {
		return links;
	}

	public String getGeoLoc() {
		return geoLoc;
	}

	public String getSocial() {
		return social;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setLinks(String links) {
		this.links = links;
	}

	public void setGeoLoc(String geoLoc) {
		this.geoLoc = geoLoc;
	}

	public void setSocial(String social) {
		this.social = social;
	}

	public void setFrequenze(double[] freq) {
		this.frequenze = freq;
	}

	public double[] getFrequenze() {
		return frequenze;
	}

	public void setSimilarita(double similarita) {
		this.similarita = similarita;
	}

	public double getSimilarita() {
		return similarita;
	}

	@Override
	public int compareTo(Post p) {
		if (this.similarita < p.getSimilarita())
			return -1;
		else if (this.similarita == p.getSimilarita())
			return 0;
		else
			return 1;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if ((o == null) || (o.getClass() != this.getClass()))
			return false;
		Post p = (Post) o;
		if (p.getText() != null&&text!=null)
			return text.equals(p.getText()) && this.nome.equals(p.getNome());
		else
			return nome.equals(p.getNome());

	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = (int) (31 * hash + similarita);
		return hash;
	}

}
