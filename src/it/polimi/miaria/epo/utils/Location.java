package it.polimi.miaria.epo.utils;


public class Location {
	private String status;
	private Results[] results;
	
	public String getAdr() {
		return results[0].getLoc();
	}
	
	public static class Results {
		String[] types;
		String formatted_address;
		Address_Components[] address_components;
		
		public String getLoc() {
			return formatted_address;
		}
		
	}
	
	public static class Address_Components {
		String long_name;
		String short_name;
		String[] types;
	}


}
