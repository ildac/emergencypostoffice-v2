package it.polimi.miaria.epo.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

@Entity
@Table(name="ricerca_symset_pesi_pirro_ex")
public class PostResultBean extends AbstractPojo{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	private double similaritÓ,perc;
	private String hash;
	
	
	public double getSimilaritÓ() {
		return similaritÓ;
	}
	public void setSimilaritÓ(double similaritÓ) {
		this.similaritÓ = similaritÓ;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public void setPerc(double perc) {
		this.perc = perc;
	}
	public double getPerc() {
		return perc;
	}
	
	
	
	
	

}
