package it.polimi.miaria.epo.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

	@Entity
	@Table(name="ricerca_5")
	public class ViewPost extends AbstractPojo{
		private static final long serialVersionUID = 1L;
		
		private String hash;
		@Id
		@GeneratedValue
		private Long id;
		private int rilevante;
		private double similaritÓ,perc;
		
		
		
		public double getSimilaritÓ() {
			return similaritÓ;
		}
		public void setSimilaritÓ(double similaritÓ) {
			this.similaritÓ = similaritÓ;
		}
		
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		
		
		public String getHash() {
			return hash;
		}
		public void setHash(String hash) {
			this.hash = hash;
		}
		public void setPerc(double perc) {
			this.perc = perc;
		}
		public double getPerc() {
			return perc;
		}
		public void setRilevante(int rilevante) {
			this.rilevante = rilevante;
		}
		public int getRilevante() {
			return rilevante;
		}
		
		
		
		
		


}
