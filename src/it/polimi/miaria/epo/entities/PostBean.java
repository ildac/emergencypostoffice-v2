package it.polimi.miaria.epo.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

@Entity
@Table(name="post_rilevanti")
public class PostBean extends AbstractPojo{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	private double peso;
	private int rilevante;
	private String testo,nome,hash,social;
	@Temporal(TemporalType.DATE)
	private Date data;
	
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public int getRilevante() {
		return rilevante;
	}
	public void setRilevante(int rilevante) {
		this.rilevante = rilevante;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTesto() {
		return testo;
	}
	public void setTesto(String testo) {
		this.testo = testo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setSocial(String social) {
		this.social = social;
	}
	public String getSocial() {
		return social;
	}
	
	
	
	

}
