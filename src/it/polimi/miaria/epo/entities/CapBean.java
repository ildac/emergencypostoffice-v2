package it.polimi.miaria.epo.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

@Entity
@Table(name="caps")
public class CapBean extends AbstractPojo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	private int letto,visualizzato;
	private String path="",nome="";
	
	@Temporal(TemporalType.DATE)
	private Date data;
	
	
	
	@Column(name="idCaps")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="path")
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	@Column(name="data")
	@Temporal(TemporalType.DATE)
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	@Column(name="nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Column(name="letto")
	public int getLetto() {
		return letto;
	}
	public void setLetto(int letto) {
		this.letto = letto;
	}
	
	@Column(name="visualizzato")
	public int getVisualizzato() {
		return visualizzato;
	}
	public void setVisualizzato(int visualizzato) {
		this.visualizzato = visualizzato;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	

}
