package it.polimi.miaria.epo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.vaadin.appfoundation.persistence.data.AbstractPojo;

@Entity
@Table(name="termini_pesati")
public class TerminiBean extends AbstractPojo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	private String evento,termine;
	private double peso;
	
	
	@Column(name="id")
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="evento",nullable=false)
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	
	@Column(name="termine",nullable=false)
	public String getTermine() {
		return termine;
	}
	public void setTermine(String termine) {
		this.termine = termine;
	}
	
	@Column(name="peso",nullable=false)
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	

}
