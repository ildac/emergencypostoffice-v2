/*
-- Query: select * from `epodb`.`termini_pesati`
LIMIT 0, 1000

-- Date: 2011-05-30 15:48
*/
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (1,0.9,1,'Severe Thunderstorm','wind');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (2,0.9,1,'Severe Thunderstorm','speed');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (3,0.8,1,'Severe Thunderstorm','lightning');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (4,0.6,1,'Severe Thunderstorm','water');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (5,0.6,1,'Severe Thunderstorm','level');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (6,0.5,1,'Severe Thunderstorm','tree');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (7,0.5,1,'Severe Thunderstorm','fire');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (8,0.2,1,'Severe Thunderstorm','house');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (9,0.7,1,'Severe Thunderstorm','roofless');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (10,0.7,1,'Severe Thunderstorm','havoc');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (11,1,1,'Severe Thunderstorm','thunderstorm');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (12,0.7,1,'Earthquake','scale');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (13,0.9,1,'Earthquake','richter');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (14,0.8,1,'Earthquake','havoc');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (15,0.9,1,'Earthquake','magnitude');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (16,0.4,1,'Earthquake','terror');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (17,0.4,1,'Earthquake','fear');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (18,0.6,1,'Earthquake','damage');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (19,0.5,1,'Earthquake','shake');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (20,0.3,1,'Earthquake','upheavel');
INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (21,0.2,1,'Earthquake','shock');

INSERT INTO `epodb`.`termini_pesati` (ID,PESO,CONSISTENCYVERSION,EVENTO,TERMINE) VALUES (22,1,1,'Earthquake','earthquake');